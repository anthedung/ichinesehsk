package com.anthedung.abcchinese.util;

import android.content.Context;
import android.util.Log;

import com.anthedung.abcchinese.R;
import com.anthedung.abcchinese.model.*;
import com.anthedung.abcchinese.model.CharacterCard;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Random;

import static com.anthedung.abcchinese.model.Constants.EASY_DECK_FILE_NAME;
import static com.anthedung.abcchinese.model.Constants.HARD_DECK_FILE_NAME;
import static com.anthedung.abcchinese.model.Constants.MASTER_DECK_FILE_NAME;
import static com.anthedung.abcchinese.model.Constants.MEDIUM_DECK_FILE_NAME;

/**
 * Created by anthedung on 8/8/15.
 */
public class DeckUtil {

    public static void addCharacterToDeck(List<CharacterCard> deck, CharacterCard c){
        if (c == null){
            // Log.e(MasterDeck.class.getName(), "null is not allowed in constructor");
            throw new IllegalArgumentException();
        } else if (getCharacterByID(deck, c.getCharacterID()) != null){
            // Log.e(MasterDeck.class.getName(), "CharacterCard Already Exists in Master Deck");
            throw new IllegalArgumentException();
        }
        deck.add(c);
    }

    public static boolean addCharacterListToMasterDeck(List<CharacterCard> deck, List<CharacterCard> deckToAdd){
        for (CharacterCard c: deckToAdd) {
            if (c == null) {
                // Log.e(MasterDeck.class.getName(), "null is not allowed in constructor");
                throw new IllegalArgumentException();
            } else if (getCharacterByID(deck, c.getCharacterID()) != null) {
                // Log.e(MasterDeck.class.getName(), "CharacterCard Already Exists in Master Deck");
                throw new IllegalArgumentException();
            }
        }

        // If all unique
        deck.addAll(deckToAdd);
        return true;
    }

    public static void removeCharacterFromDeck(List<CharacterCard> deck, CharacterCard c){
        if (c == null){
            // Log.e(MasterDeck.class.getName(), "null is not allowed in removeCharacterFromMainDeck");
            throw new IllegalArgumentException();
        }
        deck.remove(c);
    }

    public static CharacterCard getCharacterByID(List<CharacterCard> deck, int lookupID){

        for (CharacterCard c: deck) {
            if (c.getCharacterID() == lookupID)
                return c;
        }

        return null;
    }

    public static boolean isCharacterExistsInList(List<CharacterCard> cardList, int lookupID){

        for (CharacterCard c: cardList) {
            if (c.getCharacterID() == lookupID)
                return true;
        }

        return false;
    }

    public static CharacterCard generateRandomNotInReviewedList(List<CharacterCard> list, List<CharacterCard> reviewedList){
        //if Card is found in reviewed Deck != -1 => continue to randomize
        if (list != null && list.isEmpty()) {return null;}

        Random rdn = new Random();
        int High = list.size();
        int rId = rdn.nextInt(High);

        if (list.size() == reviewedList.size()) {
            rId = rdn.nextInt(High);
            return list.get(rId);
        }

        while (reviewedList.indexOf(list.get(rId)) != -1){
            rId = rdn.nextInt(High);
        }

        return list.get(rId);
    }

    // next item in list given current card
    public static CharacterCard generateNextCardInOrder(List<CharacterCard> list, CharacterCard currentCard){
        //if Card is found in reviewed Deck != -1 => continue to randomize
        if (list != null && list.isEmpty()) {return null;}

        if (currentCard == null){
            return list.get(0); // return first card
        }
        if (list.indexOf(currentCard) < list.size()-1){
            return list.get(list.indexOf(currentCard)+1);
        }
        return null;
    }

    public static CharacterCard generatePrevCardInOrder(List<CharacterCard> list, CharacterCard currentCard){
        //if Card is found in reviewed Deck != -1 => continue to randomize
        if (list != null && list.isEmpty()) return null;
        if (currentCard == null){
            return list.get(0); // return first card
        }
        if (list.indexOf(currentCard) > 0){
            return list.get(list.indexOf(currentCard)-1);
        }
        return null;
    }


    // IO Operation
    public static void writeToFile(String data, String fileName, Context ctx) {
        if (!fileName.endsWith(".json")){
            fileName = fileName.concat(".json");
        }
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(ctx.openFileOutput(fileName, Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            // Log.e("Exception", "File write failed: " + e.toString());
        }
    }


    public static String readFromFile(String fileName, Context ctx) {
        if (!fileName.endsWith(".json")){
            fileName = fileName.concat(".json");
        }

        String ret = "";

        try {
            InputStream inputStream = ctx.openFileInput(fileName);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
//             Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
//             Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }

    public static String readFromFileInAssetFolder(String fileName, Context ctx) {
        if (!fileName.endsWith(".json")){
            fileName = fileName.concat(".json");
        }

        String ret = "";

        try {
            InputStream inputStream = ctx.getAssets().open(fileName);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
//             Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
//             Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }

    public static String getLocalizedDeckNameFromName(String fromDeckName, Context ctx) {
        String fromDeckNameLocalized = fromDeckName;

        switch (fromDeckName) {
            case EASY_DECK_FILE_NAME:
                fromDeckNameLocalized = ctx.getText(R.string.easy_deck_for_toast).toString();
                break;
            case MEDIUM_DECK_FILE_NAME:
                fromDeckNameLocalized = ctx.getText(R.string.medium_deck_for_toast).toString();
                break;
            case HARD_DECK_FILE_NAME:
                fromDeckNameLocalized = ctx.getText(R.string.hard_deck_for_toast).toString();
                break;
            case MASTER_DECK_FILE_NAME:
                fromDeckNameLocalized = ctx.getText(R.string.master_deck_for_toast).toString();
                break;
        }

        return fromDeckNameLocalized;
    }

    public static boolean setDeckCurrentPositions(CharacterDeck deckInUse, CharacterCard characterCard, Context ctx){
        int currentPosition = deckInUse.getDeckCurrentPosition(characterCard);
        switch(deckInUse.getDeckName()){
            case MASTER_DECK_FILE_NAME:
                AppOptions.deckCurrentPositions.setMasterDeckCurrentPosition(currentPosition);
                break;
            case EASY_DECK_FILE_NAME:
                AppOptions.deckCurrentPositions.setEasyDeckCurrentPosition(currentPosition);
                break;
            case MEDIUM_DECK_FILE_NAME:
                AppOptions.deckCurrentPositions.setMediumDeckCurrentPosition(currentPosition);
                break;
            case HARD_DECK_FILE_NAME:
                AppOptions.deckCurrentPositions.setHardDeckCurrentPosition(currentPosition);
                break;
            default:
                return false;
        }

        // write to DeckPosition
//        Log.i("Current:",AppOptions.deckCurrentPositions.toString());
        // will find a less expensive way to keep states
//        writeToFile(AppOptions.deckCurrentPositions.toString(), APP_OPTIONS_DECK_CURRENT_POSITION_JSON, ctx);
        return true;
    }

    public static int getDeckCurrentPositionByDeckName(String deckInUseName){
        switch(deckInUseName){
            case MASTER_DECK_FILE_NAME:
                return AppOptions.deckCurrentPositions.getMasterDeckCurrentPosition();
            case EASY_DECK_FILE_NAME:
                return AppOptions.deckCurrentPositions.getEasyDeckCurrentPosition();
            case MEDIUM_DECK_FILE_NAME:
                return AppOptions.deckCurrentPositions.getMediumDeckCurrentPosition();
            case HARD_DECK_FILE_NAME:
                return AppOptions.deckCurrentPositions.getHardDeckCurrentPosition();
            default:
                return -1;
        }
    }
}
