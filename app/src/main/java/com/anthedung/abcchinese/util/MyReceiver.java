package com.anthedung.abcchinese.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.anthedung.abcchinese.service.MyAlarmService;

/**
 * Created by anthedung on 2/9/15.
 */
public class MyReceiver extends BroadcastReceiver
{

    @Override
    public void onReceive(Context context, Intent intent)
    {
        Intent service1 = new Intent(context, MyAlarmService.class);
        context.startService(service1);

    }
}

