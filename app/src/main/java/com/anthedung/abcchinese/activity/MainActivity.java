package com.anthedung.abcchinese.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.anthedung.abcchinese.Admin.AppAdmin;
import com.anthedung.abcchinese.R;
import com.anthedung.abcchinese.model.*;
import com.anthedung.abcchinese.util.DeckUtil;
import com.anthedung.abcchinese.util.MyReceiver;
import com.anthedung.abcchinese.view.GIFView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;

import java.util.Calendar;
import java.util.List;

import static com.anthedung.abcchinese.model.Constants.*;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener {

    private static Toast aToast;
    private static float centralFontSize;
    private float x1, x2;
    private float y1, y2;
    private long startTouchTime, endTouchTime;
    public static CharacterDeck deckInUse;
    CardSequence cardSequence;
    // Util
    Gson gson = new Gson();
    boolean isShowingMeaning;
    boolean isShowingUsages;
    boolean isShowingStrokes;
    boolean isShowingAboutUs;
    boolean isCentralCharacterFontChinese;

    // LAYOUT

    private TextView centralCharacterLabel;
    private TextView pinyinLabel;
    private TextView cnLabelEasy;
    private TextView enLabelEasy;
    private TextView cnLabelMedium;
    private TextView enLabelMedium;
    private TextView cnLabelHard;
    private TextView enLabelHard;

    //preference
    private CheckBox randomCheckBox;
    private CheckBox pinyinCheckBox;
    private CheckBox hanziCheckBox;
    private LinearLayout linearLayoutEasy;
    private LinearLayout linearLayoutMedium;
    private LinearLayout linearLayoutHard;
    private RelativeLayout relativeAppBodyLayout;
    private LinearLayout linearUsage;
    private LinearLayout linearUsage1;
    private TextView usage1;
    private TextView usagePinyin1;
    private TextView usageMeaning1;
    private LinearLayout linearUsage2;
    private TextView usage2;
    private TextView usagePinyin2;
    private TextView usageMeaning2;
    private LinearLayout linearUsage3;
    private TextView usage3;
    private TextView usagePinyin3;
    private TextView usageMeaning3;
    private TextView[][] usageArray;
    private ProgressBar progressBar;
    private TextView progressBarLabel;
    GIFView strokeGif;
    private SeekBar seekBar;


    private LinearLayout aboutUsLinearLayout;
    // Gesture
    private static int hoveredDownId;
    private static int levelSelectedId;

    // Alarm
    private PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        prepareUIBinding();

        setInititalOptions();
        setInitialData(); // includes mock

        /* set initial reviewed list data only after set initial options and initial data done */
        setInitialReviewedListFromSave();


        // Default card content to display
        switchToNewList(deckInUse, NO_SELECTED);

        // font size & selected
        prepareDefaultValues();

        // Set listeners
        setEventListeners();

        // Set AlarmManager:
        setAlarmManager();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // Set settings (three dots) to be invisible
        MenuItem item = menu.findItem(R.id.action_settings);
        item.setVisible(false);
        super.onPrepareOptionsMenu(menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
//            Intent intent = new Intent(this, SettingsActivity.class);
//            startActivity(intent);
            return false;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                y1 = event.getY();
                startTouchTime = System.currentTimeMillis();

                switch (v.getId()) {
                    // If Down On Level Label
                    case R.id.linearLayoutEasy:
                    case R.id.linearLayoutMedium:
                    case R.id.linearLayoutHard:
                        hoveredDownId = v.getId();
                        setLinearLayoutLevelHovered(v.getId());
                        break;
                    default:
                        break;
                }

                break;
            case MotionEvent.ACTION_UP:
                x2 = event.getX();
                y2 = event.getY();
                endTouchTime = System.currentTimeMillis();
                long touchTime = endTouchTime - startTouchTime;
                float deltaX = x1 - x2;
                float deltaY = y1 - y2;
                // System.out.println("Touch Time: " + touchTime + " & DeltaX: " + deltaX + " & DeltaY: " + deltaY);
                if (deltaX > MIN_DISTANCE) {
                    // SWIPE Right to Left
                    if (cardSequence.getNextCard() == null) {
                        if (deckInUse.isAllReviewed()) {
                            toastWithString(R.string.congratzAllReviewd);
                        }
                        break;
                    }

                    cardSequence.sextNextCardSequence(AppOptions.isRandomChecked(), deckInUse.getCharacterList(), deckInUse.getCharacterListReview());
                    setNewCardToDisplay(cardSequence.getCurrentCard(), AppOptions.isHanziChecked(), AppOptions.isPinyinChecked());

                    if (cardSequence.getNextCard() == null) {
                        toastWithString(R.string.congratzAllReviewd);
                    }


                } else if (-deltaX > MIN_DISTANCE) {
                    // SWIPE Left to right
                    if (cardSequence.getPrevCard() == null) {
                        // Nothing -> no Previous yet
                        if (deckInUse.isAllReviewed()) {
                            toastWithString(R.string.congratzAllReviewd);
                        }
                        break;
                    }
                    cardSequence.sextPrevCardSequence(AppOptions.isRandomChecked(), deckInUse.getCharacterList(), deckInUse.getCharacterListReview());
                    deckInUse.addToReviewList(cardSequence.getCurrentCard());
                    setNewCardToDisplay(cardSequence.getCurrentCard(), AppOptions.isHanziChecked(), AppOptions.isPinyinChecked());

                    if (cardSequence.getNextCard() == null) {
                        toastWithString(R.string.congratzAllReviewd);
                    }
                } else if (-deltaY > MIN_DISTANCE_Y) {
                    switch (v.getId()) {
                        case R.id.mainCharacterLayout:
                            if (!isShowingStrokes) {
                                setStrokeToDisplay();
                            } else {
                                setNewCardToDisplay(cardSequence.getCurrentCard(), AppOptions.isHanziChecked(), AppOptions.isPinyinChecked());
                            }
                            break;
                    }
                } else if (deltaY > MIN_DISTANCE_Y) {
                    switch (v.getId()) {
                        case R.id.mainCharacterLayout:
                            if (!isShowingAboutUs) {
                                setAboutUsToDisplay();
                            } else {
                                setNewCardToDisplay(cardSequence.getCurrentCard(), AppOptions.isHanziChecked(), AppOptions.isPinyinChecked());
                            }
                            break;
                    }
                } else {
                    if (touchTime > MIN_SUPER_LONG_TOUCH_TIME) // Super long to  Tap
                    {
                        DeckUtil.writeToFile(new AppOptions().toString(), APP_OPTIONS_JSON, this);
//                        Log.i("SUPER_LONG_TOUCH_TIME: ", new AppOptions().toString());
                        toastWithString(R.string.appOptionsAlreadySaved);
                        break;
                    } else if (touchTime < MIN_TOUCH_TIME) // Short Tap
                    {
                        switch (v.getId()) {
                            case R.id.mainCharacterLayout:
                                if (!isShowingMeaning) {
                                    setMeaningToDisplay(cardSequence.getCurrentCard(), AppOptions.isHanziChecked(), AppOptions.isPinyinChecked());
                                } else {
                                    setNewCardToDisplay(cardSequence.getCurrentCard(), AppOptions.isHanziChecked(), AppOptions.isPinyinChecked());
                                }
                                break;
                            case R.id.linearLayoutEasy:
                            case R.id.linearLayoutMedium:
                            case R.id.linearLayoutHard:
                                shortTapOnLevelLable(v.getId());
                                setProgressBarAndLabelAndDeckCount();
                                break;
                            default:
                                break;
                        }
                    } else { // Long Tap
                        switch (v.getId()) {
                            case R.id.mainCharacterLayout:
                                if (!isShowingUsages) {
                                    setUsagesToDisplay(cardSequence.getCurrentCard());
                                } else {
                                    setNewCardToDisplay(cardSequence.getCurrentCard(), AppOptions.isHanziChecked(), AppOptions.isPinyinChecked());
                                }

                                break;
                            case R.id.linearLayoutEasy:
                            case R.id.linearLayoutMedium:
                            case R.id.linearLayoutHard:
                                longTapOnLevelLable(v.getId());
//                                setProgressBarAndLabelAndDeckCount();
                                break;
                            default:
                                break;
                        }
                    }
                }
                break;

        }
        return true;
    }

    private void setNewCardToDisplay(CharacterCard c, boolean isHanziChecked, boolean isPinyinChecked) {
        centralCharacterLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, centralFontSize * 1.5f);

        /// Default
        isShowingUsages = false;
        isShowingMeaning = false;
        isShowingStrokes = false;
        isShowingAboutUs = false;
        linearUsage.setVisibility(LinearLayout.GONE);
        strokeGif.setVisibility(View.GONE);
        aboutUsLinearLayout.setVisibility(LinearLayout.GONE);
        centralCharacterLabel.setText(c.getCharacter());
        pinyinLabel.setText("");
        try {
            strokeGif.loadGIFResource(this, c.getStrokeIdByFileName(this));
        } catch (Exception e) {
            strokeGif.loadGIFResource(this, R.drawable.strokesdefault);
        }

        if (isHanziChecked) {
            if (centralCharacterLabel.getText().length() >= 3)
                centralCharacterLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, centralFontSize * 0.96f);
            if (!isCentralCharacterFontChinese) {
            }
            if (isPinyinChecked) {

                centralCharacterLabel.setText(c.getCharacter());
                pinyinLabel.setText("(" + c.getPinyin() + ")");
            } else {
                centralCharacterLabel.setText(c.getCharacter());
                pinyinLabel.setText("");
            }
            isCentralCharacterFontChinese = true;

        } else if (isPinyinChecked) {
            if (isCentralCharacterFontChinese) {
            }
            centralCharacterLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, centralFontSize * 0.8f);
            centralCharacterLabel.setText(c.getPinyin());
            pinyinLabel.setText("");
            isCentralCharacterFontChinese = false;
        } else {
            toastWithString(R.string.app_options_atleastinPinyinorHanzi);
        }

        deckInUse.addToReviewList(cardSequence.getCurrentCard());

        // Set Current Position: Only set current positions if NOT random
        if (!AppOptions.isRandomChecked()) {
            DeckUtil.setDeckCurrentPositions(deckInUse, cardSequence.getCurrentCard(), this);
        }

        setProgressBarAndLabelAndDeckCount();
    }

    private void setUsagesToDisplay(CharacterCard c) {
        linearUsage.setVisibility(LinearLayout.VISIBLE);
        isShowingStrokes = false;
        isShowingMeaning = false;
        isShowingUsages = true;
        isShowingAboutUs = false;
        strokeGif.setVisibility(View.GONE);
        aboutUsLinearLayout.setVisibility(LinearLayout.GONE);
        centralCharacterLabel.setText("");
        pinyinLabel.setText("");
        List<Usage> usages = c.getUsages();

        if (usages.size() == 1) { // if only 1 examle, then set at the bottom
            usageArray[2][0].setText(usages.get(0).getUsage());
            usageArray[2][1].setText(usages.get(0).getUsagePinyin());
            usageArray[2][2].setText(usages.get(0).getUsageMeaning());
        } else {
            for (int i = 0; i < usages.size() || i < 3; i++) {
                Usage tempUsage = usages.get(i);
                usageArray[i][0].setText(tempUsage.getUsage());
                usageArray[i][1].setText(tempUsage.getUsagePinyin());
                usageArray[i][2].setText(tempUsage.getUsageMeaning());
            }
        }
    }

    private void setMeaningToDisplay(CharacterCard c, boolean isHanziChecked, boolean isPinyinChecked) {
        int textLength = centralCharacterLabel.getText().length();
        if (textLength < 12)
            centralCharacterLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, centralFontSize * 0.5f);
        else if (textLength < 20) {
            centralCharacterLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, centralFontSize * 0.3f);
        } else {
            centralCharacterLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, centralFontSize * 0.25f);
        }
        isShowingMeaning = !isShowingMeaning;// to display samething when back (Character or Meaning)
        isShowingUsages = false;
        isShowingStrokes = false;
        isShowingMeaning = true;
        isShowingAboutUs = false;
        linearUsage.setVisibility(LinearLayout.GONE);
        strokeGif.setVisibility(View.GONE);
        aboutUsLinearLayout.setVisibility(LinearLayout.GONE);
        centralCharacterLabel.setText(c.getMeaning());
        pinyinLabel.setText(c.getPinyin());

        // Prepare meaning
        // options logics
        if (isHanziChecked) {
            centralCharacterLabel.setText(c.getMeaning());
            pinyinLabel.setText("(" + c.getPinyin() + ")");
        } else if (isPinyinChecked) {
            pinyinLabel.setText(c.getMeaning());
            pinyinLabel.setText("(" + c.getCharacter() + ")");
        } else {
            toastWithString(R.string.app_options_atleastinPinyinorHanzi);
        }
    }

    private void setStrokeToDisplay() {
        centralCharacterLabel.setText("");
        pinyinLabel.setText("");
        isShowingUsages = false;
        isShowingMeaning = true;
        isShowingStrokes = true;
        isShowingAboutUs = false;
        strokeGif.setVisibility(LinearLayout.VISIBLE);
        linearUsage.setVisibility(LinearLayout.GONE);
        aboutUsLinearLayout.setVisibility(LinearLayout.GONE);

    }

    private void setAboutUsToDisplay() {
        centralCharacterLabel.setText("");
        pinyinLabel.setText("");
        isShowingUsages = false;
        isShowingMeaning = false;
        isShowingStrokes = false;
        isShowingAboutUs = true;
        aboutUsLinearLayout.setVisibility(LinearLayout.VISIBLE);
        linearUsage.setVisibility(LinearLayout.GONE);
        strokeGif.setVisibility(LinearLayout.GONE);

    }

    private void labelSelected(int id) {
        switch (id) {
            case R.id.linearLayoutEasy:
                linearLayoutEasy.setBackground(getResources().getDrawable(R.drawable.zbtn_black_solid));
                cnLabelEasy.setTextColor(Color.WHITE);
                enLabelEasy.setTextColor(Color.WHITE);
                break;
            case R.id.linearLayoutMedium:
                linearLayoutMedium.setBackground(getResources().getDrawable(R.drawable.zbtn_black_solid));
                cnLabelMedium.setTextColor(Color.WHITE);
                enLabelMedium.setTextColor(Color.WHITE);
                break;
            case R.id.linearLayoutHard:
                linearLayoutHard.setBackground(getResources().getDrawable(R.drawable.zbtn_black_solid));
                cnLabelHard.setTextColor(Color.WHITE);
                enLabelHard.setTextColor(Color.WHITE);
                break;
        }
    }

    private void labelDeselected(int id) {
        switch (id) {
            case R.id.linearLayoutEasy:
                linearLayoutEasy.setBackground(getResources().getDrawable(R.drawable.zbtn_option_button_original));
                cnLabelEasy.setTextColor(Color.BLACK);
                enLabelEasy.setTextColor(Color.GRAY);
                break;
            case R.id.linearLayoutMedium:
                linearLayoutMedium.setBackground(getResources().getDrawable(R.drawable.zbtn_option_button_original));
                cnLabelMedium.setTextColor(Color.BLACK);
                enLabelMedium.setTextColor(Color.GRAY);
                break;
            case R.id.linearLayoutHard:
                linearLayoutHard.setBackground(getResources().getDrawable(R.drawable.zbtn_option_button_original));
                cnLabelHard.setTextColor(Color.BLACK);
                enLabelHard.setTextColor(Color.GRAY);
                break;
        }
    }

    private void setLinearLayoutLevelHovered(int labelId) {
        // If selected then can't hover, as can't add also
        if (hoveredDownId == levelSelectedId)
            return;

        switch (labelId) {
            case R.id.linearLayoutEasy:

                linearLayoutEasy.setBackground(getResources().getDrawable(R.drawable.zbtn_light_grey_hover_solid));
                cnLabelEasy.setTextColor(Color.BLACK);
                enLabelEasy.setTextColor(Color.BLACK);
                break;
            case R.id.linearLayoutMedium:
                linearLayoutMedium.setBackground(getResources().getDrawable(R.drawable.zbtn_light_grey_hover_solid));
                cnLabelMedium.setTextColor(Color.BLACK);
                enLabelMedium.setTextColor(Color.BLACK);
                break;
            case R.id.linearLayoutHard:
                linearLayoutHard.setBackground(getResources().getDrawable(R.drawable.zbtn_light_grey_hover_solid));
                cnLabelHard.setTextColor(Color.BLACK);
                enLabelHard.setTextColor(Color.BLACK);
                break;
        }
    }

    //Long tap action:
    private void longTapOnLevelLable(int labelId) {
        CharacterDeck deck = Decks.getDeckByLevelLableResId(labelId);
        List<CharacterCard> list = deck.getCharacterList();

        // Finish Color Hovering if hover != selected
        if (hoveredDownId != levelSelectedId || levelSelectedId == NO_SELECTED) {
            labelDeselected(hoveredDownId);
        }

        if (deck.isDeckEmpty()) {
            toastWithString(R.string.isEmptyTapToAdd, DeckUtil.getLocalizedDeckNameFromName(deck.getDeckName(), this));
            return;
        }
        // If same is being reviewed => doesn't allow to add to same list
        if (levelSelectedId == labelId) {
            switchToNewList(Decks.getMasterDeck(), NO_SELECTED);
            toastWithString(R.string.currentlyReviewing, DeckUtil.getLocalizedDeckNameFromName(Decks.getMasterDeck().getDeckName(), this));
            labelDeselected(labelId);
            return;
        } else {
            labelDeselected(levelSelectedId);
            switchToNewList(deck, labelId);
            labelSelected(labelId);
            levelSelectedId = labelId;
        }
    }

    //Short tap action:
    // 1. Dehover
    // 2. Add to new list
    // 3. If empty after move => deselect and notify
    private void shortTapOnLevelLable(int labelId) {
        CharacterDeck toDeck = Decks.getDeckByLevelLableResId(labelId);
        // localize

        CharacterCard cCard = cardSequence.getCurrentCard();
        String fromDeckName = null;

        // Finish Color Hovering if hover != selected
        if (hoveredDownId != levelSelectedId || levelSelectedId == NO_SELECTED) {
            labelDeselected(hoveredDownId);
        }

        // If same is being reviewed => go back to Master View
        if (levelSelectedId == labelId) {
            toastWithString(R.string.currentlyReviewing, DeckUtil.getLocalizedDeckNameFromName(toDeck.getDeckName(), this));
//            Toast.makeText(MainActivity.this, "Currently Reviewing " + toDeck.getDeckName(), Toast.LENGTH_SHORT).show();
            return;
        }// card existings in the same deck then not adding
        if (DeckUtil.isCharacterExistsInList(toDeck.getCharacterList(), cCard.getCharacterID())) {
            toastWithString(R.string.isAlreadyIn, cCard.getCharacter(), DeckUtil.getLocalizedDeckNameFromName(toDeck.getDeckName(), this));
//            Toast.makeText(MainActivity.this, cCard.getCharacter() + " is already in " + toDeck.getDeckName(), Toast.LENGTH_SHORT).show();
            return;
        } else {
            // get currentCard's position in the list
            int currentPosition = deckInUse.getCharacterList().indexOf(cardSequence.getCurrentCard());

            // From deck is unknown, thus search, toDeck is known. Medium most likely moved around
            if (Decks.getMediumDeck().removeFromDeck(cCard, this)) {
                fromDeckName = Decks.getMediumDeck().getDeckName();
                deckInUse.removeFromReviewDeck(cCard, this); // remove from deckInUse => remove from its review list too
            } else if (Decks.getEasyDeck().removeFromDeck(cCard, this)) {
                fromDeckName = Decks.getEasyDeck().getDeckName();
                deckInUse.removeFromReviewDeck(cCard, this);
            } else if (Decks.getHardDeck().removeFromDeck(cCard, this)) {
                fromDeckName = Decks.getHardDeck().getDeckName();
                deckInUse.removeFromReviewDeck(cCard, this);
            }

            toDeck.addToDeck(cCard, this);


            if (fromDeckName != null) { // if exist in any deck
                fromDeckName = DeckUtil.getLocalizedDeckNameFromName(fromDeckName, this);

                toastWithString(R.string.isMovedFrom, cardSequence.getCurrentCard().getCharacter(), fromDeckName, DeckUtil.getLocalizedDeckNameFromName(toDeck.getDeckName(), this));
            } else {
                toastWithString(R.string.isNewlyAddedTo, cardSequence.getCurrentCard().getCharacter(), DeckUtil.getLocalizedDeckNameFromName(toDeck.getDeckName(), this));
            }

            // Deselect empty selected
            if (levelSelectedId != NO_SELECTED) {
                CharacterDeck fromDeck = Decks.getDeckByLevelLableResId(levelSelectedId);
                fromDeckName = fromDeck.getDeckName();
                if (fromDeck.isDeckEmpty()) {
                    labelDeselected(levelSelectedId);
                    labelSelected(labelId);
                    switchToNewList(toDeck, labelId);
                }
            }

            // if move then reset card
            if (fromDeckName != null && levelSelectedId != NO_SELECTED) {
                cardSequence = new CardSequence(deckInUse, AppOptions.isRandomChecked(), currentPosition);
                setNewCardToDisplay(cardSequence.getCurrentCard(), AppOptions.isHanziChecked(), AppOptions.isPinyinChecked());
            }
        }

    }

    private void switchToNewList(CharacterDeck deck, int labelId) {
        deckInUse = deck;
        levelSelectedId = labelId;
        int currentPosition = DeckUtil.getDeckCurrentPositionByDeckName(deckInUse.getDeckName());

        if (currentPosition < 0) {
            cardSequence = new CardSequence(deckInUse, AppOptions.isRandomChecked());
        } else {
            cardSequence = new CardSequence(deckInUse, AppOptions.isRandomChecked(), currentPosition);
        }
        setNewCardToDisplay(cardSequence.getCurrentCard(), AppOptions.isHanziChecked(), AppOptions.isPinyinChecked());
        toastWithString(R.string.currentlyReviewing, DeckUtil.getLocalizedDeckNameFromName(deck.getDeckName(), this));

        linearUsage.setVisibility(LinearLayout.GONE);
        strokeGif.setVisibility(View.GONE);
    }


    private void prepareDefaultValues() {
        levelSelectedId = NO_SELECTED;
        hoveredDownId = NO_SELECTED;

        // need to show only when not random
        if (AppOptions.isRandomChecked()) {
            seekBar.setVisibility(View.GONE);
        } else {
            seekBar.setVisibility(View.VISIBLE);
        }
    }

    private void setInititalOptions() {
        String appOptionsStr = DeckUtil.readFromFile(APP_OPTIONS_JSON, this);
//        Log.i("inititalOptions str:", appOptionsStr);
        AppOptions tempAppOptions = gson.fromJson(appOptionsStr, AppOptions.class);

        if (tempAppOptions == null) {
            tempAppOptions = new AppOptions();
            DeckUtil.writeToFile(new AppOptions().toString(), APP_OPTIONS_JSON, this);
        } else {
            tempAppOptions.deckCurrentPositions = tempAppOptions.getDeckCurrentPositionsGson();
        }

        hanziCheckBox.setChecked(tempAppOptions.isHanziCheckedGson());
        pinyinCheckBox.setChecked(tempAppOptions.isPinyinCheckedGson());
        randomCheckBox.setChecked(tempAppOptions.isRandomCheckedGson());
        AppOptions.setIsHanziChecked(hanziCheckBox.isChecked());
        AppOptions.setIsPinyinChecked(pinyinCheckBox.isChecked());
        AppOptions.setIsRandomChecked(randomCheckBox.isChecked());
    }


    private void setInitialData() {
        // control for future update
        // Read from asset folders - modifiable/accessible
        String isMasterDeckUpdateOrNot = DeckUtil.readFromFileInAssetFolder(APP_ADMIN_JSON, this);
        AppAdmin appAdmin = gson.fromJson(isMasterDeckUpdateOrNot, AppAdmin.class);

        // Read from current hidden path
        String masterDeckString = DeckUtil.readFromFile(MASTER_DECK_FILE_NAME, this);
        CharacterDeck tempM = gson.fromJson(masterDeckString, CharacterDeck.class);

        if ((tempM == null) || appAdmin.isMasterDeckUpdate()) {
            masterDeckString = DeckUtil.readFromFileInAssetFolder(DEFAULT_MASTER_DECK_FILE_NAME, this);
//            System.out.println("MasterDeckString: " + masterDeckString);
            tempM = gson.fromJson(masterDeckString, CharacterDeck.class);
            Decks.setDeckByName(tempM, MASTER_DECK_FILE_NAME);
//           Log.i(MainActivity.class.getName(), "New Master Deck from " + DEFAULT_MASTER_DECK_FILE_NAME + ": " + Decks.getMasterDeck().toString());
            DeckUtil.writeToFile(Decks.getMasterDeck().toString(), MASTER_DECK_FILE_NAME, this);
        } else {
            // // Log.i(MainActivity.class.getName(), tempM.getDeckName() + " is NOT updated with size of " + tempM.getCharacterList().size());
            Decks.setDeckByName(tempM, MASTER_DECK_FILE_NAME);
        }

        //initialize 3 Decks
        CharacterDeck tempDeck = gson.fromJson(DeckUtil.readFromFile(EASY_DECK_FILE_NAME, this), CharacterDeck.class);
        if (tempDeck != null) {
            Decks.setDeckByName(tempDeck, EASY_DECK_FILE_NAME);
            // // Log.i(MainActivity.class.getName(), "New Easy Deck from json: " + tempDeck.toString());

        } else {
            DeckUtil.writeToFile(Decks.getEasyDeck().toString(), EASY_DECK_FILE_NAME, this);
            // // Log.i(MainActivity.class.getName(), "New Easy Deck from scratch: " + Decks.getEasyDeck().toString());
        }

        tempDeck = gson.fromJson(DeckUtil.readFromFile(MEDIUM_DECK_FILE_NAME, this), CharacterDeck.class);
        if (tempDeck != null) {
            Decks.setDeckByName(tempDeck, MEDIUM_DECK_FILE_NAME);
        } else {
            DeckUtil.writeToFile(Decks.getMediumDeck().toString(), MEDIUM_DECK_FILE_NAME, this);
        }

        tempDeck = gson.fromJson(DeckUtil.readFromFile(HARD_DECK_FILE_NAME, this), CharacterDeck.class);
        if (tempDeck != null) {
            Decks.setDeckByName(tempDeck, HARD_DECK_FILE_NAME);
        } else {
            DeckUtil.writeToFile(Decks.getHardDeck().toString(), HARD_DECK_FILE_NAME, this);
        }

        usageArray = new TextView[][]{
                {usage1, usagePinyin1, usageMeaning1}, {usage2, usagePinyin2, usageMeaning2}, {usage3, usagePinyin3, usageMeaning3}
        };

        deckInUse = Decks.getMasterDeck();
    }

    private void setInitialReviewedListFromSave() {
        // fill up reviewed list
        int currentPos = AppOptions.deckCurrentPositions.getMasterDeckCurrentPosition();
        if (currentPos > -1) {
            for (int i = 0; i < currentPos; i++) {
                Decks.getMasterDeck().addToReviewList(Decks.getMasterDeck().getCharacterList().get(i));
            }
        }

        currentPos = AppOptions.deckCurrentPositions.getEasyDeckCurrentPosition();
        if (currentPos > -1) {
            for (int i = 0; i < currentPos; i++) {
                Decks.getEasyDeck().addToReviewList(Decks.getEasyDeck().getCharacterList().get(i));
            }
        }

        currentPos = AppOptions.deckCurrentPositions.getMediumDeckCurrentPosition();
        if (currentPos > -1) {
            for (int i = 0; i < currentPos; i++) {
                Decks.getMediumDeck().addToReviewList(Decks.getMediumDeck().getCharacterList().get(i));
            }
        }

        currentPos = AppOptions.deckCurrentPositions.getHardDeckCurrentPosition();
        if (currentPos > -1) {
            for (int i = 0; i < currentPos; i++) {
                Decks.getHardDeck().addToReviewList(Decks.getHardDeck().getCharacterList().get(i));
            }
        }
    }


    private void prepareUIBinding() {
        // UI Binding
        strokeGif = (GIFView) findViewById(R.id.stokeGifCustomView);
        relativeAppBodyLayout = (RelativeLayout) findViewById(R.id.mainCharacterLayout);
        aboutUsLinearLayout = (LinearLayout) findViewById(R.id.linearAboutus);

        linearLayoutEasy = (LinearLayout) findViewById(R.id.linearLayoutEasy);
        linearLayoutMedium = (LinearLayout) findViewById(R.id.linearLayoutMedium);
        linearLayoutHard = (LinearLayout) findViewById(R.id.linearLayoutHard);
        centralCharacterLabel = (TextView) findViewById(R.id.centralCharacter);
        centralFontSize = getResources().getDimension(R.dimen.text_central_Character);


        pinyinLabel = (TextView) findViewById(R.id.pinyinLabel);
        cnLabelEasy = (TextView) findViewById(R.id.easyLevel);
        enLabelEasy = (TextView) findViewById(R.id.easyEnLevel);
        cnLabelMedium = (TextView) findViewById(R.id.mediumLevel);
        enLabelMedium = (TextView) findViewById(R.id.mediumEnLevel);
        cnLabelHard = (TextView) findViewById(R.id.hardLevel);
        enLabelHard = (TextView) findViewById(R.id.hardEnLevel);
        // Linear Usage
        linearUsage = (LinearLayout) findViewById(R.id.linearUsage);
        linearUsage1 = (LinearLayout) findViewById(R.id.linearUsage1);
        usage1 = (TextView) findViewById(R.id.usage1);
        usagePinyin1 = (TextView) findViewById(R.id.usagePinyin1);
        usageMeaning1 = (TextView) findViewById(R.id.usageMeaning1);
        linearUsage2 = (LinearLayout) findViewById(R.id.linearUsage2);
        usage2 = (TextView) findViewById(R.id.usage2);
        usagePinyin2 = (TextView) findViewById(R.id.usagePinyin2);
        usageMeaning2 = (TextView) findViewById(R.id.usageMeaning2);
        linearUsage3 = (LinearLayout) findViewById(R.id.linearUsage3);
        usage3 = (TextView) findViewById(R.id.usage3);
        usagePinyin3 = (TextView) findViewById(R.id.usagePinyin3);
        usageMeaning3 = (TextView) findViewById(R.id.usageMeaning3);

        // Options
        randomCheckBox = (CheckBox) findViewById(R.id.checkBoxRandom);
        pinyinCheckBox = (CheckBox) findViewById(R.id.checkBoxPinyin);
        hanziCheckBox = (CheckBox) findViewById(R.id.checkBoxHanzi);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBarLabel = (TextView) findViewById(R.id.progressNoLabel);

        seekBar = (SeekBar) findViewById(R.id.seekBar);

        //Google Adview
        AdView mAdView = (AdView) findViewById(R.id.adView);

        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("74D181975D82DF14DBD69B11605FEDDA")
                .addTestDevice("TEST_EMULATOR")
                .build();
        mAdView.loadAd(adRequest);
    }

    private void setEventListeners() {
        // Set event listener
        relativeAppBodyLayout.setOnTouchListener(this);
        linearLayoutEasy.setOnTouchListener(this);
        linearLayoutMedium.setOnTouchListener(this);
        linearLayoutHard.setOnTouchListener(this);

        hanziCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton button, boolean isChecked) {
                AppOptions.setIsHanziChecked(isChecked);
            }
        });

        hanziCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton button, boolean isHanziChecked) {
//                // System.out.println("Hanzi Check: " + isHanziChecked);
                if (!isHanziChecked && !AppOptions.isPinyinChecked()) {
                    toastWithString(R.string.app_options_atleastinPinyinorHanzi);
                    hanziCheckBox.setChecked(!isHanziChecked);
                } else {
                    AppOptions.setIsHanziChecked(isHanziChecked);
                    setNewCardToDisplay(cardSequence.getCurrentCard(), AppOptions.isHanziChecked(), AppOptions.isPinyinChecked());
                }
            }
        });

        pinyinCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton button, boolean isPinyinChecked) {
//                // System.out.println("Pinyin Check: " + isPinyinChecked);
                if (!AppOptions.isHanziChecked() && !isPinyinChecked) {
                    toastWithString(R.string.app_options_atleastinPinyinorHanzi);
                    pinyinCheckBox.setChecked(!isPinyinChecked);
                } else {
                    AppOptions.setIsPinyinChecked(isPinyinChecked);
                    setNewCardToDisplay(cardSequence.getCurrentCard(), AppOptions.isHanziChecked(), AppOptions.isPinyinChecked());
                }
            }
        });

        randomCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton button, boolean isChecked) {
                AppOptions.setIsRandomChecked(isChecked);

                if (AppOptions.isRandomChecked()) {
                    seekBar.setVisibility(View.GONE);
                } else {
                    seekBar.setVisibility(View.VISIBLE);
                }

                switchToNewList(deckInUse, levelSelectedId);
            }

        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                progress = progresValue;
//                Toast.makeText(getApplicationContext(), "Changing seekbar's progress", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
//                Toast.makeText(getApplicationContext(), "Started tracking seekbar", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int currentPosition = Math.round((float) deckInUse.getCharacterList().size() * progress / seekBar.getMax());
                cardSequence = new CardSequence(deckInUse, false, currentPosition); // for seek bar always NON-Random
                setNewCardToDisplay(cardSequence.getCurrentCard(), AppOptions.isHanziChecked(), AppOptions.isPinyinChecked());

                toastWithString(R.string.showCharacterSeekBar, currentPosition+"");
            }
        });
    }

    private void setAlarmManager(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 21);
        calendar.set(Calendar.MINUTE, 45);

//        calendar.set(Calendar.AM_PM, Calendar.PM);
        // if it's already past 10PM => set nextday
        if (calendar.before(Calendar.getInstance())){
            calendar.add(Calendar.DATE, 1);
        }

        Intent myIntent = new Intent(MainActivity.this, MyReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, myIntent, 0);

        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC,calendar.getTimeInMillis(),AlarmManager.INTERVAL_DAY,pendingIntent);
    }

    private void setProgressBarAndLabelAndDeckCount() {

        //setProgressBarAndLabel();
        progressBar.setProgress(deckInUse.getPercentageReviewed());
        progressBarLabel.setText(deckInUse.getPercentageReviewed() + "%");

        // setDeckCountDisplay();
        enLabelEasy.setText("(" + Decks.getEasyDeck().getCharacterList().size() + ")");
        enLabelMedium.setText("(" + Decks.getMediumDeck().getCharacterList().size() + ")");
        enLabelHard.setText("(" + Decks.getHardDeck().getCharacterList().size() + ")");

        int currentPos = DeckUtil.getDeckCurrentPositionByDeckName(deckInUse.getDeckName());
        seekBar.setProgress((int) (((float) currentPos / deckInUse.getCharacterList().size()) * seekBar.getMax()));
    }

    // Toast with string format
    private void toastWithString(int mainBodyID, String... formatComponents) {
        if (aToast != null) {
            aToast.cancel();
        }
        aToast = Toast.makeText(MainActivity.this, String.format(getText(mainBodyID).toString(), formatComponents), Toast.LENGTH_SHORT);
        TextView v = (TextView) aToast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        aToast.show();
    }
}