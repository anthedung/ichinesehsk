package com.anthedung.abcchinese.service;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.anthedung.abcchinese.R;
import com.anthedung.abcchinese.activity.MainActivity;
import com.anthedung.abcchinese.model.CharacterCard;
import com.anthedung.abcchinese.model.CharacterDeck;
import com.anthedung.abcchinese.model.Decks;
import com.anthedung.abcchinese.util.DeckUtil;
import com.google.gson.Gson;

import static com.anthedung.abcchinese.model.Constants.DEFAULT_MASTER_DECK_FILE_NAME;

/**
 * Created by anthedung on 2/9/15.
 */
public class MyAlarmService extends IntentService {

    private NotificationManager mManager;

    public MyAlarmService(){
        super("MyAlarmService");
    }

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
    }

    @Override
    public void onHandleIntent(Intent intent) {
        sendNotification();
    }

    private  void sendNotification(){

        // get a random character
        String masterDeckString = DeckUtil.readFromFileInAssetFolder(DEFAULT_MASTER_DECK_FILE_NAME, this);
        CharacterDeck tempM = new Gson().fromJson(masterDeckString, CharacterDeck.class);
        CharacterCard characterCard = DeckUtil.generateRandomNotInReviewedList(tempM.getCharacterList(),
                Decks.getMasterDeck().getCharacterListReview());
//        Log.i("AlarmService", characterCard.getCharacter());

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), PendingIntent.FLAG_CANCEL_CURRENT);

        mManager = (NotificationManager) this.getApplicationContext().getSystemService(this.getApplicationContext().NOTIFICATION_SERVICE);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.abc_chinese_logo_white_all)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setContentTitle(this.getText(R.string.noti_doyouknow))
                .setSubText(String.format(this.getText(R.string.noti_clicktoopenapp).toString(), this.getText(R.string.app_name)))
                .setContentText(String.format(this.getText(R.string.noti_wordoftheday).toString(), characterCard.getCharacter(), characterCard.getPinyin(), characterCard.getMeaning()));

//        Log.i("intent.getStringExtra", intent.getStringExtra("wordoftheday"));

        notificationBuilder.setContentIntent(contentIntent);

        mManager.notify(1, notificationBuilder.build());
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

}






