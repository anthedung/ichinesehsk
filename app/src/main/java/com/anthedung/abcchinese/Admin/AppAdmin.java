package com.anthedung.abcchinese.Admin;

/**
 * Created by anthedung on 17/8/15.
 */
public class AppAdmin {
    boolean isMasterDeckUpdate;
    boolean isNewFeatureRelease;

    public boolean isNewFeatureRelease() {
        return isNewFeatureRelease;
    }

    public void setIsNewFeatureRelease(boolean isNewFeatureRelease) {
        this.isNewFeatureRelease = isNewFeatureRelease;
    }

    public boolean isMasterDeckUpdate() {
        return isMasterDeckUpdate;
    }

    public void setIsMasterDeckUpdate(boolean isUpdate) {
        isMasterDeckUpdate = isUpdate;
    }
}
