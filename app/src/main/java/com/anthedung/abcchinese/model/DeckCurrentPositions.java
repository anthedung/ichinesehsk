package com.anthedung.abcchinese.model;

import com.google.gson.Gson;

/**
 * Created by anthedung on 22/8/15.
 */
public class DeckCurrentPositions {
    private int masterDeckCurrentPosition = -1;
    private int easyDeckCurrentPosition = -1;
    private int mediumDeckCurrentPosition = -1;
    private int hardDeckCurrentPosition = -1;

    public int getMasterDeckCurrentPosition() {
        return masterDeckCurrentPosition;
    }

    public void setMasterDeckCurrentPosition(int masterDeckCurrentPosition) {
        this.masterDeckCurrentPosition = masterDeckCurrentPosition;
    }

    public int getEasyDeckCurrentPosition() {
        return easyDeckCurrentPosition;
    }

    public void setEasyDeckCurrentPosition(int easyDeckCurrentPosition) {
        this.easyDeckCurrentPosition = easyDeckCurrentPosition;
    }

    public int getMediumDeckCurrentPosition() {
        return mediumDeckCurrentPosition;
    }

    public void setMediumDeckCurrentPosition(int mediumDeckCurrentPosition) {
        this.mediumDeckCurrentPosition = mediumDeckCurrentPosition;
    }

    public int getHardDeckCurrentPosition() {
        return hardDeckCurrentPosition;
    }

    public void setHardDeckCurrentPosition(int hardDeckCurrentPosition) {
        this.hardDeckCurrentPosition = hardDeckCurrentPosition;
    }



    @Override
    public String toString(){
        return new Gson().toJson(this);
    }
}
