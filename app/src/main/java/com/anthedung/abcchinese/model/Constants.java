package com.anthedung.abcchinese.model;

/**
 * Created by anthedung on 9/8/15.
 */
public class Constants {
    public static final int MIN_DISTANCE = 150;
    public static final int MIN_DISTANCE_Y = 120;
    public static final int MIN_TOUCH_TIME = 250;
    public static final int MIN_SUPER_LONG_TOUCH_TIME = 1000;
    public static final int NO_SELECTED = -1;

    public static final String MASTER_DECK_FILE_NAME = "Master Deck";
    public static final String EASY_DECK_FILE_NAME = "Easy Deck";
    public static final String MEDIUM_DECK_FILE_NAME = "Medium Deck";
    public static final String HARD_DECK_FILE_NAME = "Hard Deck";

    public static final String DEFAULT_MASTER_DECK_FILE_NAME = "MasterDeckDefault.json";

    public static final String APP_ADMIN_JSON = "AppAdmin.json";

    public static final String APP_OPTIONS_DECK_CURRENT_POSITION_JSON = "AppOptions_DeckCurrentPosition.json";
    public static final String APP_OPTIONS_JSON = "AppOptions.json";


}
