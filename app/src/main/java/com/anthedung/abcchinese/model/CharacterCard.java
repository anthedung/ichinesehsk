package com.anthedung.abcchinese.model;

import android.content.Context;
import android.util.Log;

import com.anthedung.abcchinese.util.DeckUtil;

import java.io.Serializable;
import java.util.List;

/**
 * Created by anthedung on 8/8/15.
 */
public class CharacterCard implements Serializable {

    private int characterID;
    private String character;
    private String pinyin;

    private String meaning;
    private List<Usage> usages;
    private String strokeOrder; //http://www.yellowbridge.com/chinese/character-stroke-order.php?word=國

    //Constructor
    public CharacterCard(int characterID, String character, String pinyin, String meaning, String strokeOrder, List<Usage> usages){
        if (character == null || character.isEmpty()) {
            // Log.e(CharacterCard.class.getName(), "null is not allowed in constructor");
            throw new IllegalArgumentException();
        } else if (DeckUtil.getCharacterByID(MasterDeck.MASTER_DECK, characterID) != null){
            // Log.e(CharacterCard.class.getName(), "CharacterCard Already Exists in Master Deck");
            throw new IllegalArgumentException();
        }

        this.characterID = characterID;
        this.character = character;
        this.pinyin = pinyin;
        this.meaning = meaning;
        this.strokeOrder = strokeOrder;
        this.usages = usages;
    }

    //Getters & Setters
    public void setCharacterID(int characterID) {
        this.characterID = characterID;
    }

    public int getCharacterID() {
        return characterID;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        if (character == null || character.isEmpty()){
            // Log.e(CharacterCard.class.getName(), "null is not allowed in setCharacter");
            throw new IllegalArgumentException();
        }
        this.character = character;
    }

    public String getPinyin() {
        return pinyin;
    }

    public void setPinyin(String pinyin) {
        this.pinyin = pinyin;
    }


    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }


    public List<Usage> getUsages() {
        return usages;
    }

    public void setUsages(List<Usage> usages) {
        this.usages = usages;
    }

    public String getStroke() {
        return strokeOrder;
    }

    public int getStrokeIdByFileName(Context ctx){
        return ctx.getResources().getIdentifier(this.strokeOrder, "drawable", ctx.getPackageName());
    }

    public void setStroke(String stroke) {
        this.strokeOrder = stroke;
    }

    @Override
    public boolean equals(Object anotherCharacter) {
        if (anotherCharacter == null) return false;

        if (anotherCharacter instanceof CharacterCard
                && (this.characterID == ((CharacterCard) anotherCharacter).getCharacterID()
                || this.character.equals(((CharacterCard) anotherCharacter).getCharacter())))
            return true;
        else
            return false;
    }

    @Override
    public String toString() {
        return this.character;
    }
}
