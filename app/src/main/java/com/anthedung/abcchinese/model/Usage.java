package com.anthedung.abcchinese.model;

/**
 * Created by anthedung on 8/8/15.
 */
public class Usage {
    String usage;
    String usagePinyin;
    String usageMeaning;

    public Usage(String usage, String usagePinyin, String usageMeaning) {
        this.usage = usage;
        this.usagePinyin = usagePinyin;
        this.usageMeaning = usageMeaning;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    public String getUsagePinyin() {
        return usagePinyin;
    }

    public void setUsagePinyin(String usagePinyin) {
        this.usagePinyin = usagePinyin;
    }

    public String getUsageMeaning() {
        return usageMeaning;
    }

    public void setUsageMeaning(String usageMeaning) {
        this.usageMeaning = usageMeaning;
    }
}
