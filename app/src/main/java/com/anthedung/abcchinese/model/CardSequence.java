package com.anthedung.abcchinese.model;

import android.util.Log;

import com.anthedung.abcchinese.util.DeckUtil;

import java.util.List;

/**
 * Created by anthedung on 8/8/15.
 */
public class CardSequence {
    private CharacterCard currentCard;
    private CharacterCard prevCard;
    private CharacterCard nextCard;

    // initiate new CardSequence once
    public CardSequence(CharacterDeck deckInUse, boolean isTrulyRandom) {
        if (deckInUse == null) {
            // Log.e(CardSequence.class.getName(), "Cannot initiatize");
//            throw new IllegalArgumentException("List is Null");
        }

        if (deckInUse.isDeckEmpty()) {
            // Log.e(CardSequence.class.getName(), "List is Empty");
            //throw new IllegalArgumentException("List is Empty");
        }

        if (deckInUse.getCharacterList().size() < deckInUse.getCharacterListReview().size()) {
            // Log.e(CardSequence.class.getName(), "Reviewed list cannot be larger than current list");
//            throw new IllegalArgumentException("Reviewed list cannot be larger than current list");
        }


        //initialize completely 3 cards
        if (isTrulyRandom) {
            currentCard = DeckUtil.generateRandomNotInReviewedList(deckInUse.getCharacterList(), deckInUse.getCharacterListReview());
            deckInUse.addToReviewList(currentCard);
            nextCard = DeckUtil.generateRandomNotInReviewedList(deckInUse.getCharacterList(), deckInUse.getCharacterListReview());
            prevCard = DeckUtil.generateRandomNotInReviewedList(deckInUse.getCharacterList(), deckInUse.getCharacterListReview()); // No Prev card yet
        } else {
            currentCard = DeckUtil.generateNextCardInOrder(deckInUse.getCharacterList(), currentCard);
            deckInUse.addToReviewList(currentCard);
            nextCard = DeckUtil.generateNextCardInOrder(deckInUse.getCharacterList(), currentCard);
            prevCard = null; // No Prev card yet
        }
    }

    // Initiate new CardSequence with current position
    public CardSequence(CharacterDeck deckInUse, boolean isTrulyRandom, int currentPosition) {
        /* Logics checks for UAT
        if (deckInUse == null) {
             Log.e(CardSequence.class.getName(), "Cannot initiatize with current position");
            throw new IllegalArgumentException("List is Null");
        }

        if (deckInUse.isDeckEmpty()) {
             Log.e(CardSequence.class.getName(), "List is Empty  with current position");
            throw new IllegalArgumentException("List is Empty");
        }

        if (deckInUse.getCharacterList().size() < deckInUse.getCharacterListReview().size()){
             Log.e(CardSequence.class.getName(), "Reviewed list cannot be larger than current list");
            throw new IllegalArgumentException("Reviewed list cannot be larger than current list");
        }
*/
        //initialize completely 3 cards
        if (isTrulyRandom) {
            currentCard = DeckUtil.generateRandomNotInReviewedList(deckInUse.getCharacterList(), deckInUse.getCharacterListReview());
            deckInUse.addToReviewList(currentCard);
            nextCard = DeckUtil.generateRandomNotInReviewedList(deckInUse.getCharacterList(), deckInUse.getCharacterListReview());
            prevCard = DeckUtil.generateRandomNotInReviewedList(deckInUse.getCharacterList(), deckInUse.getCharacterListReview()); // No Prev card yet
        } else {
            if (currentPosition >= deckInUse.getCharacterList().size()) {
                currentCard = deckInUse.getCharacterList().get(deckInUse.getCharacterList().size() - 1);
                nextCard = null;
                if (deckInUse.getCharacterList().size() - 2 >= 0) {
                    prevCard = deckInUse.getCharacterList().get(deckInUse.getCharacterList().size() - 2);
                } else {
                    prevCard = null;
                }
            } else {
                currentCard = deckInUse.getCharacterList().get(currentPosition);
                deckInUse.addToReviewList(currentCard);
                if (currentPosition + 1 < deckInUse.getCharacterList().size()) {
                    nextCard = deckInUse.getCharacterList().get(currentPosition + 1);
                } else
                    nextCard = null;

                if (currentPosition - 1 > 0) {
                    prevCard = deckInUse.getCharacterList().get(currentPosition);
                } else {
                    prevCard = null;
                }
            }
        }
    }

    public CharacterCard getCurrentCard() {
//        if (isTrulyRandom)
//            return (currentCard == null) ?
//                    DeckUtil.generateRandomNotInReviewedList(list, reviewedList) : currentCard;
//        else
//            return (currentCard == null) ?
//                    DeckUtil.generateNextCardInOrderNotInReviewedList(list, reviewedList) : currentCard;
        return currentCard;
    }

    private void setCurrentCard(CharacterCard c) {
//        if (c == null || list == null)
//            currentCard = DeckUtil.generateRandomNotInReviewedList(list, reviewedList);
//        else
//            this.currentCard = c;
        currentCard = c;
    }

    public CharacterCard getPrevCard() {
//        return (prevCard == null) ?
//                DeckUtil.generateRandomCharacterCard(list) : prevCard;
        return prevCard;
    }

    private void setPrevCard(CharacterCard c) {
//        if (c == null || list == null)
//            prevCard = DeckUtil.generateRandomCharacterCard(list);
//        else
//            prevCard = c;
        prevCard = c;
    }

    public CharacterCard getNextCard() {
//        return (nextCard == null) ?
//                DeckUtil.generateRandomCharacterCard(list) : nextCard;
        return nextCard;
    }

    private void setNextCard(CharacterCard c) {
//        if (c == null || list == null)
//            nextCard = DeckUtil.generateRandomCharacterCard(list);
//        else
//            nextCard = c;
        nextCard = c;
    }

    public void sextNextCardSequence(boolean isTrulyRandom, List<CharacterCard> list, List<CharacterCard> listReviewed) {
        this.setPrevCard(this.currentCard);
        this.setCurrentCard(this.nextCard);

        if (isTrulyRandom) {
            // generate next Card
            nextCard = DeckUtil.generateRandomNotInReviewedList(list, listReviewed);
        } else {
            nextCard = DeckUtil.generateNextCardInOrder(list, currentCard);
        }
    }

    public void sextPrevCardSequence(boolean isTrulyRandom, List<CharacterCard> list, List<CharacterCard> listReviewed) {
        this.setNextCard(this.currentCard);
        this.setCurrentCard(this.prevCard);

        if (isTrulyRandom) {
            // generate next Card
            prevCard = DeckUtil.generateRandomNotInReviewedList(list, listReviewed);
        } else {
            prevCard = DeckUtil.generatePrevCardInOrder(list, currentCard);
        }
    }

}
