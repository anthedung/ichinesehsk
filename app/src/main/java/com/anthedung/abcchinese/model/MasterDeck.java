package com.anthedung.abcchinese.model;

import android.util.Log;

import com.anthedung.abcchinese.util.DeckUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anthedung on 8/8/15.
 */
public class MasterDeck {
    public static final List<CharacterCard> MASTER_DECK = new ArrayList<>();

    public static boolean addCharacterListToMasterDeck(List<CharacterCard> deckToAdd){
        for (CharacterCard c: deckToAdd) {
            if (c == null) {
                // Log.e(MasterDeck.class.getName(), "null is not allowed in constructor");
                throw new IllegalArgumentException();
            } else if (DeckUtil.getCharacterByID(MASTER_DECK, c.getCharacterID()) != null) {
                // Log.e(MasterDeck.class.getName(), "CharacterCard Already Exists in Master Deck");
                throw new IllegalArgumentException();
            }
        }

        // If all unique
        MASTER_DECK.addAll(deckToAdd);
        return true;
    }
}
