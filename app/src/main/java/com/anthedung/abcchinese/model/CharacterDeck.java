package com.anthedung.abcchinese.model;

import android.content.Context;
import android.util.Log;

import com.anthedung.abcchinese.util.DeckUtil;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anthedung on 8/8/15.
 */
public class CharacterDeck {
    private String deckName; //for custom decks if any

    private List<CharacterCard> characterList = new ArrayList<>();
    private List<CharacterCard> characterListReview = new ArrayList<>();

    public List<CharacterCard> getCharacterListReview() {
        if (characterListReview == null){
            characterListReview = new ArrayList<>();
        }
        return characterListReview;
    }

    public List<CharacterCard> getCharacterList() {
        return characterList;
    }

    public void clearReviewList() {
        if (characterListReview == null){
            characterListReview = new ArrayList<>();
            return;
        }
        characterListReview.clear();
    }

    public CharacterDeck(String deckName) {
        this.deckName = deckName;
    }

    public String getDeckName() {
        return deckName;
    }

    public void setDeckName(String deckName) {
        this.deckName = deckName;
    }

    public boolean addToDeck(CharacterCard c, Context ctx) {
        if (DeckUtil.isCharacterExistsInList(characterList, c.getCharacterID())) {
            // Log.i(CharacterDeck.class.getName(), "Not added since " + c.getCharacter() + " already exists in " + this.deckName);
            return false;
        } else {
            characterList.add(c);
            DeckUtil.writeToFile(this.toString(), this.deckName, ctx);
            // Log.i(CharacterDeck.class.getName(), "Added and saved to file: " + c.getCharacter() + " to " + this.deckName);

            return true;
        }
    }

    public boolean removeFromDeck(CharacterCard c, Context ctx) {
        if (!DeckUtil.isCharacterExistsInList(characterList, c.getCharacterID())) {
            return false;
        } else {
            characterList.remove(c);
            DeckUtil.writeToFile(this.toString(), this.deckName, ctx);
            // Log.i(CharacterDeck.class.getName(), "Removed and saved to file: " + c.getCharacter() + " to " + this.deckName);

            return true;
        }
    }

    public boolean removeFromReviewDeck(CharacterCard c, Context ctx) {
//        if (!DeckUtil.isCharacterExistsInList(characterList, c.getCharacterID())) {
//            return false;
//        } else {
            characterListReview.remove(c);
//            DeckUtil.writeToFile(this.toString(), this.deckName, ctx);
//            // Log.i(CharacterDeck.class.getName(), "Removed and saved to file: " + c.getCharacter() + " to " + this.deckName);

            return true;
//        }
    }

    public boolean isDeckEmpty() {
        if (this.characterList != null) {
            return this.characterList.isEmpty();
        } else {
            return false;
        }
    }

    public boolean isDecReviewedkEmpty() {
        if (this.characterList != null) {
            return this.characterListReview.isEmpty();
        } else {
            return false;
        }
    }

    public String toString() {
        return new Gson().toJson(this);
    }

    public boolean isAllReviewed(){
        return (characterList.size() == characterListReview.size());
    }

    public void addToReviewList(CharacterCard characterCard){
        if (this.characterListReview != null) {
            if (!DeckUtil.isCharacterExistsInList(this.characterListReview, characterCard.getCharacterID())) {
                this.characterListReview.add(characterCard);
            }
        } else {
            this.characterListReview = new ArrayList<>();
            this.characterListReview.add(characterCard);
        }
    }

    public int getPercentageReviewed(){
        return characterListReview.size()*100/characterList.size();
    }

    public int getDeckCurrentPosition(CharacterCard characterCard){
        return characterList.indexOf(characterCard);
    }
}
