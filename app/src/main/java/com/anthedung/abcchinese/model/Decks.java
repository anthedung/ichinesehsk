package com.anthedung.abcchinese.model;

import com.anthedung.abcchinese.R;

import static com.anthedung.abcchinese.model.Constants.*;

/**
 * Created by anthedung on 8/8/15.
 */
public class Decks {
    private static CharacterDeck MASTER_DECK = new CharacterDeck(MASTER_DECK_FILE_NAME);
    private static CharacterDeck EASY_DECK = new CharacterDeck(EASY_DECK_FILE_NAME);
    private static CharacterDeck MEDIUM_DECK = new CharacterDeck(MEDIUM_DECK_FILE_NAME);

    private static CharacterDeck HARD_DECK = new CharacterDeck(HARD_DECK_FILE_NAME);

    public static CharacterDeck getMasterDeck() {
        return MASTER_DECK;
    }

    public static CharacterDeck getEasyDeck() {
        return EASY_DECK;
    }

    public static CharacterDeck getMediumDeck() {
        return MEDIUM_DECK;
    }

    public static CharacterDeck getHardDeck() {
        return HARD_DECK;
    }

    private static void setHardDeck(CharacterDeck hardDeck) {
        HARD_DECK = hardDeck;
    }

    private static void setMediumDeck(CharacterDeck mediumDeck) {
        MEDIUM_DECK = mediumDeck;
    }

    private static void setEasyDeck(CharacterDeck easyDeck) {
        EASY_DECK = easyDeck;
    }

    private static void setMasterDeck(CharacterDeck masterDeck) {
        MASTER_DECK = masterDeck;
    }

    public static CharacterDeck getDeckByLevelLableResId(int id) {
        switch (id) {
            case R.id.linearLayoutEasy:
                return EASY_DECK;
            case R.id.linearLayoutMedium:
                return MEDIUM_DECK;
            case R.id.linearLayoutHard:
                return HARD_DECK;
            default:
                return MASTER_DECK;
        }
    }

    public static void setDeckByName(CharacterDeck deck, String deckName) {
        switch (deckName){
            case MASTER_DECK_FILE_NAME:
                setMasterDeck(deck);
                break;
            case EASY_DECK_FILE_NAME:
                setEasyDeck(deck);
                break;
            case MEDIUM_DECK_FILE_NAME:
                setMediumDeck(deck);
                break;
            case HARD_DECK_FILE_NAME:
                setHardDeck(deck);
                break;
        }
    }


}