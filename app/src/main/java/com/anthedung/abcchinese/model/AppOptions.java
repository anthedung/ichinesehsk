package com.anthedung.abcchinese.model;

import com.google.gson.Gson;

/**
 * Created by anthedung on 11/8/15.
 */
public class AppOptions {
    private static boolean isHanziChecked = true;
    private static boolean isPinyinChecked;
    private static boolean isRandomChecked = true;
    public static DeckCurrentPositions deckCurrentPositions = new DeckCurrentPositions();

    private boolean isHanziCheckedGson = true;
    private boolean isPinyinCheckedGson;
    private boolean isRandomCheckedGson = true;
    private DeckCurrentPositions DeckCurrentPositionsGson;

    public boolean isHanziCheckedGson() {
        return isHanziCheckedGson;
    }

    public boolean isPinyinCheckedGson() {
        return isPinyinCheckedGson;
    }

    public boolean isRandomCheckedGson() {
        return isRandomCheckedGson;
    }

    public com.anthedung.abcchinese.model.DeckCurrentPositions getDeckCurrentPositionsGson() {
        return DeckCurrentPositionsGson;
    }

    public static boolean isHanziChecked() {
        return isHanziChecked;
    }

    public static void setIsHanziChecked(boolean isHanziChecked) {
        if(!isHanziChecked && !isPinyinChecked) {
                // // Log.i(AppOptions.class.getName(), AppOptions.info());
                throw new IllegalArgumentException("At least Hanzi or Pinyin should be checked");
        }
        AppOptions.isHanziChecked = isHanziChecked;
    }

    public static boolean isPinyinChecked() {
        return isPinyinChecked;
    }

    public static void setIsPinyinChecked(boolean isPinyinChecked) {
        if(!isHanziChecked && !isPinyinChecked){
            // // Log.i(AppOptions.class.getName(), AppOptions.info());
            throw new IllegalArgumentException("At least Hanzi or Pinyin should be checked");
        }
        AppOptions.isPinyinChecked = isPinyinChecked;
    }

    public static boolean isRandomChecked() {
        return isRandomChecked;
    }

    public static void setIsRandomChecked(boolean isRandomChecked) {
        AppOptions.isRandomChecked = isRandomChecked;
    }

    public static String info(){
        return AppOptions.class.getName()+": HanziChecked: "+isHanziChecked + " PinyinChecked: "+isPinyinChecked + " RandomChecked: " +isRandomChecked;
    }

    @Override
    public String toString(){
        this.DeckCurrentPositionsGson = deckCurrentPositions;
        this.isHanziCheckedGson = isHanziChecked;
        this.isPinyinCheckedGson = isPinyinChecked;
        this.isRandomCheckedGson = isRandomChecked;
        return new Gson().toJson(this);
    }
}
